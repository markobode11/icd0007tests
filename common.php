<?php

require_once __DIR__ . '/vendor/simpletest/simpletest/unit_tester.php';
require_once __DIR__ . '/vendor/simpletest/simpletest/web_tester.php';

const MAX_POINTS = 5;
const RESULT_PATTERN = "\nRESULT: %s of %s POINTS\n";
const SKIPPED_PATTERN = "\nSelected tests passed. Skipped %s tests.\n";

class HwTests extends WebTestCase {

    public function runWithReporter() {
        $reporter = new PointsReporter();

        $totalCount = count($this->getAllTestMethodNames());
        $selectedCount = count($this->getSelectedTestMethodNames());

        if ($selectedCount > 0) {
            $reporter->setSkipCount($totalCount - $selectedCount);
        }

        return parent::run($reporter);
    }

    public function getTests() {
        return empty($this->getSelectedTestMethodNames())
            ? $this->getAllTestMethodNames()
            : $this->getSelectedTestMethodNames();
    }

    private function getSelectedTestMethodNames() {
        $methodNames = $this->getAllTestMethodNames();

        return array_filter($methodNames, function ($each) {
            return preg_match('/^_/', $each);
        });
    }

    private function getAllTestMethodNames() {
        $class = get_class($this);

        $r = new ReflectionClass($class);

        $testMethods = array_filter($r->getMethods(), function ($each) use ($class) {
            return $each->class === $class && $each->isPublic();
        });

        return array_map(
            function ($each) {
                return $each->name;
            }, $testMethods);
    }

    public function assertFrontControllerLink($linkId) {

        $href = $this->getBrowser()->getLinkHrefById($linkId);

        $pattern = '/^(index\.php)?\?[-=&\w]*$/';

        $message = 'Front Controller pattern expects all links '
            . 'to be in ?key1=value1&key2=... format. But this link was: ' . $href;

        $this->assert(new PatternExpectation($pattern), $href, $message);
    }

    public function assertAttribute($attribute, $value) {
        $pattern = '/' . $attribute . '\s*=\s*["\']' . $value . '["\']/';

        $this->assertPattern($pattern,
            "can't find element with attribute '$attribute' and value '$value'");
    }

    public function assertNoField($fieldName) {
        $value = $this->getBrowser()->getField($fieldName);

        if ($value !== NULL) {
            $this->assertTrue(false, "field '$fieldName' should not exist");
        }
    }

    public function getLinkLabelById($id) {
        return $this->getBrowser()->getLinkLabelById($id);
    }

    public function getFieldValue($name) {
        return $this->getBrowser()->getField($name);
    }

}

class PointsReporter extends TextReporter {

    private $maxPoints;
    private $failCountBeforeTest;
    private $passedMethodCount = 0;
    private $totalMethodRunCount = 0;
    private $scale;

    public function __construct($maxPoints, $scale) {
        parent::__construct();
        $this->maxPoints = $maxPoints;
        $this->scale = $scale;
    }


    public function paintMethodStart($test_name) {
        $this->failCountBeforeTest =
            $this->getFailCount() + $this->getExceptionCount();

        parent::paintMethodStart($test_name);
    }

    public function paintMethodEnd($test_name) {
        $this->totalMethodRunCount++;

        if ($this->failCountBeforeTest < $this->getFailCount() + $this->getExceptionCount()) {
            printf("%s failed\n", $test_name);
        } else {
            $this->passedMethodCount++;
            printf("%s ok\n", $test_name);
        }

        parent::paintMethodEnd($test_name);
    }

    public function paintFooter($test_name) {
        $keys = array_keys($this->scale);
        $maxThreshold = array_pop($keys);

        if ($this->totalMethodRunCount < $maxThreshold) {
            return; // do not show points if some test are disabled
        }

        $finalPoints = 0;

        foreach ($this->scale as $threshold => $points) {
            if ($this->passedMethodCount >= $threshold) {
                $finalPoints = $points;
            }
        }

        printf(RESULT_PATTERN, $finalPoints, $this->maxPoints);
    }
}

class Author {
    public $firstName;
    public $lastName;
    public $grade;
}

class Book {
    public $title;
    public $grade;
    public $isRead;
}

function getSampleAuthor() {
    $author = new Author();
    $randomValue = substr(md5(mt_rand()), 0, 9);
    $author->firstName = $randomValue . '0';
    $author->lastName = $randomValue . '1';
    $author->grade = 5;
    return $author;
}

function getSampleBook() {
    $book = new Book();
    $randomValue = substr(md5(mt_rand()), 0, 9);
    $book->title = $randomValue . '0';
    $book->grade = 5;
    $book->isRead = true;
    return $book;
}
