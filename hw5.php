<?php

require_once 'common.php';

const BASE_URL = 'http://localhost:8080/';

class Hw5Tests extends HwTests {

    function testsAreNotReadyYet() {
        $this->fail();
    }
}

(new Hw5Tests())->run(new PointsReporter(0, []));
